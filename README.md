
# JAVA-REACT-EXAMPLE

- an example app for playing with deployment methods by **"Techworld with Nana"**


### build with 

```
podman run \
    --rm \
    -v $PWD:/home/gradle/project \
    -w /home/gradle/project \
    gradle:jdk8 \
    gradle build
```

- this results into an artefact named `/build/bin/libs/project.jar` which can be deployed on any machine with java-8 (I guess `jre` should be sufficient) available, via `java -jar project.jar`
